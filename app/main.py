import os

# Image
import base64
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO

# flask
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

# S3
import s3

# logger
from myLogger import logger

# Environment variables
MYSQL_HOST = os.environ['MYSQL_HOST']
MYSQL_USER = os.environ['MYSQL_USER']
MYSQL_PASSWORD = os.environ['MYSQL_PASSWORD']
MYSQL_PORT = int(os.environ['MYSQL_PORT'])
MYSQL_DB = os.environ['MYSQL_DB']


def image_to_data_uri(img):
    buffered = BytesIO()
    img.save(buffered, 'JPEG')
    img_base64 = base64.b64encode(buffered.getvalue())
    data_uri_byte = bytes("data:image/jpeg;base64,",
                          encoding='utf-8') + img_base64
    data_uri_string = data_uri_byte.decode('utf-8')
    return data_uri_string


def draw_box(img, lt_corner, rb_corner, index):
    draw = ImageDraw.Draw(img)
    draw.rectangle([lt_corner, rb_corner], outline="red", width=2)
    draw.text(lt_corner, str(index), font=ImageFont.truetype(
        "font/RobotoMono-Bold.ttf", size=16))
    return img


def get_s3_image(uri):
    img_stream = s3.get_file_stream(uri)
    return Image.open(img_stream)


def get_latest_result_all():
    cursor = mysql.get_db().cursor(cursor=DictCursor)
    query_latest = ("SELECT branch_id, camera_id, epoch "
                    "FROM data "
                    "ORDER BY epoch DESC "
                    "LIMIT 1;")
    cursor.execute(query_latest)
    row = cursor.fetchone()

    query_all_result = ("SELECT branch_id, camera_id, filepath, epoch,"
                        "       gender, gender_confident, race, race_confident,"
                        "       position_top, position_left, position_right, position_bottom, position_left "
                        "FROM data "
                        "WHERE branch_id=%s AND camera_id=%s AND epoch=%s;")
    cursor.execute(query_all_result,
                   (row['branch_id'], row['camera_id'], row['epoch']))
    rows = cursor.fetchall()
    cursor.close()
    return rows


app = Flask(__name__)
app.config['MYSQL_DATABASE_HOST'] = MYSQL_HOST
app.config['MYSQL_DATABASE_PORT'] = MYSQL_PORT
app.config['MYSQL_DATABASE_USER'] = MYSQL_USER
app.config['MYSQL_DATABASE_PASSWORD'] = MYSQL_PASSWORD
app.config['MYSQL_DATABASE_DB'] = MYSQL_DB
CORS(app)
api = Api(app)
mysql = MySQL()
mysql.init_app(app)


class HelloAPI(Resource):
    def get(self):
        return "Hello"


class ResultAPI(Resource):
    def get(self):
        logger.info('GET /_api/result/latest')

        rows = get_latest_result_all()
        original_image = get_s3_image(rows[0]["filepath"])

        image_with_box = original_image
        results = []
        for index, row in enumerate(rows):
            image_with_box = draw_box(image_with_box, (row['position_left'], row['position_top']), (
                row['position_right'], row['position_bottom']), index)
            results.append({
                'gender': {
                    'gender': row['gender'],
                    'confidence': row['gender_confident']
                },
                'race': {
                    'race': row['race'],
                    'confidence': row['race_confident']
                }
            })

        return {'epoch': rows[0]['epoch'],
                'branch_id': rows[0]['branch_id'],
                'camera_id': rows[0]['camera_id'],
                'results': results,
                'photo_data_uri': image_to_data_uri(image_with_box)}

        # # DEBUG
        # return {"epoch": 1574600655, "branch_id": 0, "camera_id": 0, "results": [{"gender": {"gender": "Male", "confidence": 0.9243270061280943}, "race": {"race": "Asian", "confidence": 0.7260892118967954}}], "photo_data_uri": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGFhYTAzMDAwMDgxMDUwMDAwZmYwNzAwMDA4NjA4MDAwMDJjMDkwMDAwMWMwYjAwMDA0ODBlMDAwMGMzMGUwMDAwNmQwZjAwMDAyYzEwMDAwMDY2MTUwMDAwAP/iAhxJQ0NfUFJPRklMRQABAQAAAgxsY21zAhAAAG1udHJSR0IgWFlaIAfcAAEAGQADACkAOWFjc3BBUFBMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD21gABAAAAANMtbGNtcwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmRlc2MAAAD8AAAAXmNwcnQAAAFcAAAAC3d0cHQAAAFoAAAAFGJrcHQAAAF8AAAAFHJYWVoAAAGQAAAAFGdYWVoAAAGkAAAAFGJYWVoAAAG4AAAAFHJUUkMAAAHMAAAAQGdUUkMAAAHMAAAAQGJUUkMAAAHMAAAAQGRlc2MAAAAAAAAAA2MyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHRleHQAAAAARkIAAFhZWiAAAAAAAAD21gABAAAAANMtWFlaIAAAAAAAAAMWAAADMwAAAqRYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9jdXJ2AAAAAAAAABoAAADLAckDYwWSCGsL9hA/FVEbNCHxKZAyGDuSRgVRd13ta3B6BYmxmnysab9908PpMP///9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8IAEQgAlgCWAwAiAAERAQIRAf/EABsAAAEFAQEAAAAAAAAAAAAAAAIBAwQFBgAH/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAEDAgQFBv/EABoBAAMBAQEBAAAAAAAAAAAAAAABAwIEBQb/2gAMAwAAARECEQAAAd30dBnXTQHFceUbRuVaLHszaiseVWdwigmSEGVldgjcQJWgB1WQIIFwV44Pns2rWCZc1eNzdj5F6SFiswHkUjckoGm8pzMQdygq99xmgKe5YF5Seg0M65+2jsQtS+hZnZW52Ce6sGgdjsQRRCtuqnMWOJV1t8pDLrQZxAOfC3epjyYcbz9LQ3l+R4HgrGIxKjoCTEkjjjOEb0iIMxxgh3gO6pM3efpaNU3tdNHj785YYyP28Hp1l49LefXEYeWme5tFwsWZQi8vZyKFRBSZLmNZJ6GY9nofLrzm6qiPyX51b0Of0ab0XxX1PJZNOs4OcjcO04m6LsXsfPjOfbdZYPEBok5RKHKPb4iTHyBf0HM9gapZF5Tked8689o2uvI+bekeXpRI8lhoBRTfICjcVpxIk7mh0F3ZSsw2tH6vFvmfP2fl/Q9TaUPX5l8o9SyBnKMa2Pm2YU42k4PKxDBAPhUVvqsF6XxdPYneZzc6spraXosaVD6o8nJlKw6cOrN4P1HzXRCIC6IkKiBqhAfonnHo3Ba5rbOJVV0Kdnay/8QAKhAAAQMEAQQCAgEFAAAAAAAAAQACAwQQERIFEyExQRQgIjIGFSM0QkP/2gAIAQAAAQUC2XXwp5d2YK6abGAumCtAtVi+FixyiEHOasylFq1WFj6VVdDTn+tQ7Q10E1u6yVkrJXddMrLWpziVut1ut1sVsq+vIDz3ke5Ml1dxnIlbLdNy5aYReAiSVi2CsFYKwVgqvkMFLIpu7oo3PNPx4ZFUN+JUcQ51RRBjGJ0qzn6T5K2K2K2K2KGy5ZjpKbJMvxppVx0UcSnfgcnFJJHSRfGplosAI3d3CwtV4RcneZKSM1lW1QsIdUMErhC0tHixRKzfCyi5FNBcnNAbLCC6dwxlOkBMDtnLVFqIKOU06kgOTuyLsrymwuKOjEZCbe5WLpCNTEytpY8JjhgJyKdaEohNgWzWJzyUb/8AOkq/lxyE65TeTfT1MXLRZZyVO5NlEi2RsDgg5Bfn6ZVVXR06k5R8sdHK6CqZJ1AQqzvV+VnCp6h1PLTvbPCQiu6p3ZF/VdyBJeQ4sKyc0MvVG3apIdP4R7r1w1Z8afOUUUHarF+Ym6VJJ+S8g2jcYyzlX6IeajjjBGj+vFz/ACKIo2F/5A/8j2Pu5TsWgkbLQvaWu9cNVfHnLhY2Nuedmtk/dGwt6QmlaxxJKC46pbNTGQB7GF4Jvy5zyDu7U3wbj6DuqXjnOT9YWtlDpXyMiBRtXnatHgr/AGdcWFuMbExjz+Vb/jRVAhllnNW8o2lG0nr25Zz9MrKyqaqMbaYPnhxmF7Oo4Rao2Kn4qTFDxpc6tooolKwAfYpg/uUmGxSb9cxNjc4I+CbsPcxNkdyNFGWyM0Kz9G+WnElJ3a39quMdaWKnbD//xAAkEQABAwQBBAMBAAAAAAAAAAABAAIRAxAgIRITMTJBIiNRYf/aAAgBAhEBPwGFGUW5DFrS5fxTC5YlCl+qNKsI3lod0Xj0qT50bVzJxL0bUX/OLP73i5TQuPtMra2js5FDLqQgwmxQx6epTm/ZC52KbTJ2ojCntFsGMKPiqgwoqr5W/8QAJhEAAQIGAQMFAQAAAAAAAAAAAQARAgMQEiAhBDJBURMiJDFSM//aAAgBAREBPwHN1f4XvxiiEKu06cn6VnnEIzPCfal71kZn52hDMJeIqZC2wmUoaeoTKx+qszpTqHpGAxjk+EAwoaPmLn3SHjXBwVFPggLE0GXqbZQRfHdDh/o0CMwDSBfDkGwhle4EQV1Ziknthy+yk/zVoX//xAAuEAABAwEHAwMEAgMAAAAAAAABAAIRIQMQEiAiMVEwQWETUnEEIzIzQoGRodH/2gAIAQAABj8CuiLq3bdOnRwuJc/hqj03oYX14z1oqV6DrOwOru7/AIu6qFKDXmWnntfQLUVp6Ln99gsPO5VBAWlfdEkotb+O4Vm477LVUrTlEZi1olzdULmimDhUYXB3JC0NLnJhcyHAwmWQ/iLq9Rlo2k2lWqGuLR4TZeXRyocTThYWg7yhPTqqBVNULXu2t2KKdkNk35zSpF9FWgXuK4vjsnObuexWHCxoO8BT2G2ci77hhaBlk9laO2h8Qqtlfif7VrZ2muzmnhamvC/YR8haXSMs5qy53ARaAGg7o4dnbhUutPm9r2f2OU21szpOQjLDHYW+F3n5USvIUyBaDcXPI2nJ6b/12n+jkpkpu84VOSWb88JzbRoNKOF1U93qh2EYttxcCmO7ihz/AE7PMo5hBmlzRaOticGCAxFp3uwuP230K3zNHtavnoYG2jg3iVJqbwHRjZQqAVO2S08U6cDdYrfSParMNaAITYFShjcG5LY+enLWjFzcHe1yDz2WK0McDJanz04QfirwrVruFI2yn0bYHw4K0b9Ww0/EytM/5Ut6AbymtHZN9MAg7pze4MKubUJUsGE9BpHKCCdQaqlC0cHiTGlf/8QAJRABAAICAgMBAAIDAQEAAAAAAQARITFBURBhcYGRobHh8CDR/9oACAEAAAE/ISnMybudnQllszQEVWhYBoJTwrwOY7jjRHCMLqfmiczDiPpPiV6hfUB6lLA3mT7NiftqVQR8pT+Sxoj1x649MzdQUt0j2CJ9R6S0VLeIcpQ6Dx69o5dVnljQpDgIgCxJmfyOSfcYtZGc1fqaNN6wjBuPmp6p6oStv5mHR/RudCcASvZPUxR2Ri7QMsDnAs9zLKFZQAi3uV5FF0VPd/4IsmFxB2qCVAm1dSo/cCoN7TFVDLRdSpDJpu7iL9A1y8xFYS4hKRfAxXXi7qHeUQPEul7mRDRdvMYD1HmBLU23KuItOpiooot5uCi2rMsjcXmXcqAOIk5icRcqdddsSFvGofscaKu63N8WG2oxV+J8DtKuHvG4fFHKII2QjRcs6Y3KAqgsyH7oDwfSejdEHM16sAaRgkstGxn7XIQgUoahm+YL5hrnyLDbfpO/MryI6JUr9JuWLxUUOFv8mqUxHBxN6/EycF7gG0fp/rLil/GMUI9kMEh6YLuOMS42Sr7RFM8+Psekxf8Ax+WcZ/jbXVxkf9xBvX0jO81FbgKZ4i1Ufpx8B1LOQ/x6iwVNpcvEI5motJcBEzuAwWLFzc5Slvpr3MBGtDK4YD3e4p9Kgv0sJcc0ucvf9tzBZzB4MrixuL6n2NW6D/8AUv7p/JBf5LXli7d1y+JZkKMCmL/c0Wo5lUGJBFnMcXHfKJaDf83HKRZbliy5++P/ABMR0zXqx3GuGDBiD9Qxp68NXCyQcbr5AFQYRjqTUr8B4Z1EW5aJXhc9fM2PGUcM7dxJhuOSEEMguhhLFqbWcTBn97AOmEMrol6YSoirGfjH9IaOyOSK0dTaXLzF4NeC0BVoOYxl/kP3qJw+uDkgbY1TKccLFF4+5Yp3xDH35aI/G059QcIj7lH3BYv9DE/+YzpD4EV+TbG3/mXULGg8yghGEAS1xili2cx3YxHXRdfzMXDHEgbFzDcWZCXsmIA5xH+YIHQQwe9TWTFrF7HiEvw+KlHBuO8qNcwTWxbwRGRlxAqlfshrymf0YtxDiICBuuZjbuWwt4vxnjvuFcrMorUGY5KlsvAQ0qkZf5n/2gAMAwAAARECEQAAECZjLvbGgBTmiQLdMtfhZwE46NyP9Xeru4wVufbgqgwgvX8DKr2e/rGf72Jd6nm8sgXJT+zxPi0UHqcQ/wDd0Dvzx3SckA3YHshDumf/xAAfEQEBAQACAgMBAQAAAAAAAAABABEQMSFBIFFhgbH/2gAIAQIRAT8QgLC8FpZZwQO78eDLfEs54kx1JinrjLOOkg5TPjIjXfOwtl4NWWLDKz0dWvohEPVnA+rJHq09zhBhXq3sH3CwHwLDZM1nyxBg7z1YI+A8XTbuzL8vHqep0xILQ5fWzGyThzpf0kvxwFfEZiWseMsnnHq0nYfU8hBp8Ojf5Qt//8QAIBEBAQEAAgICAwEAAAAAAAAAAQARIUEQMSBRgaHB8P/aAAgBAREBPxDI4m4hLbZxIeDti7PhmwXMA0eLoLDy9sAws8Le1x7HXKQOHry2y9t1jX+7i2I+j+sY+6xnMpt34Zc4z9Y9y2ADDwFrf0LG22Wm+A5yTLrJN25JZcebYVkHM/B7cc6zdk3iE5NSaTe0z8DeRavuCfnq1NTZkHua1ANPgbPf3K7EXLlZm9pu3Hl9R5VzGw/sv//EACQQAQACAgICAgMBAQEAAAAAAAEAESExQVFhcYGREKGx0cHh/9oACAEAAAE/EA6qlqGAsFldxN21O1HUosHxPLcEgWB8SjQSpwRBxAVwRXETslRsiNDOCWJLQZujTzEuX1KckWsfpMeUscoOz6iOUTaQI9ldESKZasAfuegaxvqXgha5IfkPEmLqCXohQME6v3sxB9CNeAR6CWeI+Ej2H1LeyVVYvC15N+XEXMcqzLyvbfMwgLAtZdbtlNU+5bV6rhW18RdXkeOZzU3NG3fEosZ0glAeWPZmZLY03StToxTiLcfjL4w21J1guOXEtYZWyo9sb7SMUc+fctNwUyyqBlkuiGw5RlQ8fE2fodUwP1MQt1Sw8By4KWHaNEAjrgJU7/Ce6U6ZbgbljLR5liqJ88k+pQAYPQ89SsGLWCTwSgDc1d+pglDAD8sKIqDNtBfuYUqnDBl93OfrAcq/E6BOqE6lnccBcN3GuILwamfP4lPggMG2WD7TPvLLAM0P+aldaZSH7ePUrRrC5fb/ACLrzFNg8kYFDnYAlXrURDZB7VAHFQ0uydky1WI2dTNggOYb28wvQ8EU8IvQ8zaHwCX4kw0GZ6Ir+mvRKHiSj2S92OA3o9tbuUdnSiqfcXUQQGnzMu3Mx3JdEFeRJlwMXgeYLIf0irrf9g2aHRLgnoLgwpyO0yCHnSD1fwEJstsQbAnZ5lecyg6jdrmU3YOviV5TB1o5y6+JVhWAYX/yErFMME4H3F0LL01LEwQFW4A0AHAl1E7QXZmL99os3viYs6mXRiFYoSrV9bR3Tc+GGXl3F2omr3ii2vQEVyRIhcXh5XTGiuza1fTC6F0JlD4IN+Yrbl/NzxhC8SxghcLiba9stZUUrEe9JwRZP4fZ7MH9j4BwZuz/AAR9SnhP+zDUhk2QdUHIAnPpQ+iEB6E2SzFOLjVS9nkKZJNU7fK8jAbSMmouiqibS7PTLDd1OhWVTcJAAtXQEd2RGU9WvnqVIktSX7l+wVbYuXOsNjNe6lqnQ5I3KBK0IaROh/Zk0rV4gRyKmCvY1FRSRQuCwfKqfiUMMCyXQA7iNaUpm5VmXH0jngQZX2Trf6H7jDPTjiZsSjhgm+YDF4jhqULNSyjjdr+jiPDXTdDCmpQWtuXuV3AlqujlgSsGMJQbqqb+5Szgpj2iNhOJ+kVp/wBMyjmLglwR1cLLdlUZotYpvof9iQeYrsmhOv08THE0wbAeGI8X6hKarGSByrBvTu8uMyu4Ngyq9hhkY5dywS5VInZBfMq5SMSXuP8AkuHwwjVr7jiwK7GUsLSKIDMQlwD0qsw4BX5isHTM06JVF2MYgcw4L1FioiHFWBeWI1xlWvtl3aWfjmUViGTJhRAbe1T1M2a8Wblg1cUbKI+meBD6BFaZGfPJbO1HUVhSxLMfP4yRyZltJEVkodq9Qf1Uv/kfuUoCAwsp+dQOwJQzUGlhRQ/Fdz8QCyzmUW2VHw1LPMJVtS4/FSphLi+yZKi1ElpdKtv1MnuSy/niClJWc3Yxvgf9IQBLJS1a1Mv+9Z/sCKGssvlwGYPGiLvygczxUI9owsFGyNiJcATJuBM6gFuXiLFXHUUc/UUO1l6IeBdGBQ4uVBUIq0ervDF6s3uQ/wBju6mnicqWW6xGLUELHJcyjrY3wv8AkxCSj3UHbiWzVdpggLdbEJHEOwkK+YnzL8MyrglCxdPcMDkD1CI4TO3zLwoMRocOfIwoRfOh74leDK7sM2mYvGJeI6B3HUOlEYCBYtQaEaKB8RsSFagXMqxa/DRbMjEBas/UBKmst3Lb2hdwTDWL4i0XCLtef5DeuWl2lukVqf/Z"}


api.add_resource(ResultAPI, '/_api/result/latest')
api.add_resource(HelloAPI, '/_api/hello')
